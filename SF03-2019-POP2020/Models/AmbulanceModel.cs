﻿namespace SF03_2019_POP2020.Models
{
    public class AmbulanceModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Address Address { get; set; }

        public bool Active { get; set; }

        public AmbulanceModel()
        {
            Address = new Address();
        }

        public override string ToString()
        {
            return "The name of Ambulance is  " + Name + " and its located at : " + Address;
        }
    }
}
