﻿using System;
using System.ComponentModel;
using System.Linq;

namespace SF03_2019_POP2020.Models
{
    [Serializable]
    public class User
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public string JMBG { get; set; }

        public Address Address { get; set; }

        public EGender Gender { get; set; }

        public EUserType UserType { get; set; }

        public bool Active { get; set; }

        public override string ToString()
        {
            return " I am " + Username + " type: " + UserType; //+", Mine adress is " + Adress;
        }

        public User()
        {
            Address = new Address();
        }

        public User Clone()
        {
            User clone = new User();

            clone.Username = Username;
            clone.Name = Name;
            clone.Surname = Surname;
            clone.Address = Address;
            clone.Email = Email;
            clone.Password = Password;
            clone.JMBG = JMBG;
            clone.Active = Active;
            clone.Gender = Gender;

            return clone;
        }
    }
}
