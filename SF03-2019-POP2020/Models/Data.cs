﻿namespace SF03_2019_POP2020.Models
{
    public sealed class Data
    {
        public static string ConnectionString = @"Server=localhost;Database=pop;
                                                    Integrated Security=True;
                                                    Connect Timeout=30;Encrypt=False;
                                                    TrustServerCertificate=False;ApplicationIntent=ReadWrite;
                                                    MultiSubnetFailover=False";

        private static readonly Data instance = new Data();

        public static Data Instance
        {
            get
            {
                return instance;
            }
        }
    }
}
