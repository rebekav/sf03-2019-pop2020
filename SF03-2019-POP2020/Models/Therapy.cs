﻿namespace SF03_2019_POP2020.Models
{
    public class Therapy
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public Doctor Doctor { get; set; }

        public bool Active { get; set; }

        public override string ToString()
        {
            return Doctor + ";" + Description;
        }
    }
}
