﻿using System;
using System.Collections.Generic;

namespace SF03_2019_POP2020.Models
{
    [Serializable]
    public class Doctor : User
    {
        public AmbulanceModel Ambulance { get; set; }

        public List<Appointment> appointments { get; set; }

        public override string ToString()
        {
            return " I'm " + Username + " and Im working " + Ambulance;
        }
    }
}
