﻿using System.Collections.Generic;

namespace SF03_2019_POP2020.Models
{
    public class Patient : User
    {
        public List<Appointment> Appointments { get; set; }

        public List<Therapy> Therapies { get; set; }


        public override string ToString()
        {
            return " I'm " + Username;
        }


    }
}
