﻿using System;

namespace SF03_2019_POP2020.Models
{
    public class Appointment
    {
        public int Id { get; set; }

        public Doctor Doctor { get; set; }

        public Patient Patient { get; set; }

        public DateTime Date { get; set; }

        public EStatusApp Status { get; set; }

        public bool Active { get; set; }

    }
}
