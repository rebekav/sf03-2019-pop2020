﻿namespace SF03_2019_POP2020.Models
{
    public class Address
    {
        public int Id { get; set; }

        public string StreetAndNumber { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public bool Active { get; set; }

        public override string ToString()
        {
            return " Street " + StreetAndNumber + " City " + City + " Country " + Country;
        }
    }
}
