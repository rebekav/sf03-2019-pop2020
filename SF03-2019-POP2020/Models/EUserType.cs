﻿namespace SF03_2019_POP2020.Models
{
    public enum EUserType
    {
        ADMIN,
        DOCTOR,
        PATIENT
    }
}
