﻿using SF03_2019_POP2020.Models;
using SF03_2019_POP2020.Services;
using SF03_2019_POP2020.Windows;
using SF03_2019_POP2020.Windows.Admin;
using SF03_2019_POP2020.Windows.Patient;
using SF03_2019_POP2020.Windows.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF03_2019_POP2020
{

    public partial class HomeWindow : Window
    {
        public HomeWindow()
        {
            InitializeComponent();
        }

        private void logIn(object sender, RoutedEventArgs e)
        {
            String jmbg = jmbgTextBox.Text;
            String password = passwordTextBox.Text;

            if (jmbg.All(char.IsDigit))
            {
                UserService userService = new UserService();
                string type = userService.logIn(jmbg, password);
                if (type != null)
                {
                    if (type == EUserType.ADMIN.ToString())
                    {
                        AdminMain adminMain = new AdminMain(jmbg);
                        adminMain.Show();
                    }
                    else if (type == EUserType.DOCTOR.ToString())
                    {

                    }
                    else
                    {
                        PatientMain patientMain = new PatientMain(jmbg);
                        patientMain.Show();
                    }
                    this.Close();
                }
                else
                {
                    error.Content = "Invalid log in credentials!";
                    jmbgTextBox.Clear();
                    passwordTextBox.Clear();
                }
            }
            else
            {
                error.Content = "Invalid JMBG!";
                jmbgTextBox.Clear();
                passwordTextBox.Clear();
            }
        }

        private void register(object sender, RoutedEventArgs e)
        {
            UserDetails userDetailsEditWindow = new UserDetails(null, EUserType.PATIENT.ToString());
            userDetailsEditWindow.ShowDialog();
        }

        private void guestUser(object sender, RoutedEventArgs e)
        {

        }
    }
}
