﻿using SF03_2019_POP2020.Models;
using System.Data;

namespace SF03_2019_POP2020.Interfaces
{
    interface ITherapyService
    {
        bool addTherapy(Therapy therapy);
        bool deleteTherapy(int id);
        DataTable getTherapies();
        bool updateTherapy(Therapy therapy);
    }
}
