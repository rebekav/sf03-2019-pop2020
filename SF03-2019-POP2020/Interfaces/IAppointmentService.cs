﻿using SF03_2019_POP2020.Models;
using System.Data;

namespace SF03_2019_POP2020.Interfaces
{
    interface IAppointmentService
    {
        bool addAppointment(Appointment appointment);
        bool deleteAppointment(int id);
        DataTable getAppointments();
        bool updateAppointment(Appointment appointment);
    }
}
