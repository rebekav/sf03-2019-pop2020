﻿using SF03_2019_POP2020.Models;
using System.Data;

namespace SF03_2019_POP2020.Interfaces
{
    interface IAmbulanceService
    {
        bool addAmbulance(AmbulanceModel ambulance);
        bool deleteAmbulance(int id);
        DataTable getAmbulances();
        bool updateAmbulance(AmbulanceModel ambulance);
        bool isAmbulanceValid(AmbulanceModel ambulance);
        DataTable getAmbulance(int id);
    }
}
