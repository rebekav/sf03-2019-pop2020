﻿using SF03_2019_POP2020.Models;
using System.Data;

namespace SF03_2019_POP2020.Services
{
    public interface IUserService
    {
        string logIn(string jmbg, string password);
        DataTable getUserDetails(string field, bool isId);
        bool registerUser(User user, string type);
        bool deleteUser(int id);
        bool updateUser(User user);
        bool isUserValid(User user);
        DataTable getUsers();
    }
}
