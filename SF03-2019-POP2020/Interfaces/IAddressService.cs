﻿using SF03_2019_POP2020.Models;
using System.Data;

namespace SF03_2019_POP2020.Services
{
    interface IAddressService
    {
        bool addAddress(Address address);
        bool deleteAddress(int id);
        DataTable getAddresses();
        bool updateAddress(Address address);
    }
}
