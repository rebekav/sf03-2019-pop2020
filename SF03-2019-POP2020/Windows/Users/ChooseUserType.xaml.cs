﻿using SF03_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF03_2019_POP2020.Windows.Users
{
    /// <summary>
    /// Interaction logic for ChooseUserType.xaml
    /// </summary>
    public partial class ChooseUserType : Window
    {
        public ChooseUserType()
        {
            InitializeComponent();
        }

        private void patient(object sender, RoutedEventArgs e)
        {
            this.Close();
            UserDetails userDetailsEditWindow = new UserDetails(null, EUserType.PATIENT.ToString());
            userDetailsEditWindow.ShowDialog();
        }

        private void doctor(object sender, RoutedEventArgs e)
        {
            this.Close();
            UserDetails userDetailsEditWindow = new UserDetails(null, EUserType.DOCTOR.ToString());
            userDetailsEditWindow.ShowDialog();
        }

        private void admin(object sender, RoutedEventArgs e)
        {
            this.Close();
            UserDetails userDetailsEditWindow = new UserDetails(null, EUserType.ADMIN.ToString());
            userDetailsEditWindow.ShowDialog();
        }
    }
}
