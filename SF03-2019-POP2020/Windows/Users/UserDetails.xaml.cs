﻿using SF03_2019_POP2020.Models;
using SF03_2019_POP2020.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF03_2019_POP2020.Windows.Users
{
    /// <summary>
    /// Interaction logic for UserDetails.xaml
    /// </summary>
    public partial class UserDetails : Window
    {
        bool isEdit = false;
        string userType = "";
        public UserDetails(DataTable user, string userType)
        {
            InitializeComponent();
            isEdit = user != null;
            this.userType = userType;
            AddressService addressService = new AddressService();
            DataTable addressTable = addressService.getAddresses();
            int userAddress = 0;
            foreach (DataRow addressRow in addressTable.Rows)
            {
                address.Items.Add(addressRow[0]);
                //if (user != null && addressRow["id"] == user.Rows[0]["address"])
                //{
                //    userAddress = index;
                //}
                //index++;
            }
            address.SelectedIndex = userAddress;

            jmbg.Text = user != null ? user.Rows[0]["jmbg"].ToString() : "";
            jmbg.IsEnabled = user != null ? false : true;
            name.Text = user != null ? user.Rows[0]["name"].ToString() : "";
            surname.Text = user != null ? user.Rows[0]["surname"].ToString() : "";
            email.Text = user != null ? user.Rows[0]["email"].ToString() : "";
            password.Text = user != null ? user.Rows[0]["password"].ToString() : "";

            if (user != null)
            {
                string gender = user.Rows[0]["gender"].ToString();
                if (gender == EGender.MALE.ToString())
                {
                    male.IsChecked = true;
                }
                else
                {
                    female.IsChecked = true;
                }
            }
            else
            {
                male.IsChecked = true;
            }
        }

        private void cancel(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void save(object sender, RoutedEventArgs e)
        {
            UserService userService = new UserService();
            User user = new User();
            user.JMBG = jmbg.Text;
            user.Name = name.Text;
            user.Surname = surname.Text;
            user.Email = email.Text;
            user.Password = password.Text;
            user.Address.StreetAndNumber = address.SelectedItem.ToString();
            if ((bool)male.IsChecked)
            {
                user.Gender = EGender.MALE;
            }
            else
            {
                user.Gender = EGender.FEMALE;
            }
            bool success = false;
            if (userService.isUserValid(user))
            {
                if (isEdit)
                {
                    success = userService.updateUser(user);
                }
                else
                {
                    success = userService.registerUser(user, userType);
                }
            }
            else
            {
                error.Content = "Invalid data!";
            }
            if (success)
            {
                this.Close();
            }
        }
    }
}
