﻿using SF03_2019_POP2020.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF03_2019_POP2020.Windows.Users
{
    /// <summary>
    /// Interaction logic for ChooseUser.xaml
    /// </summary>
    public partial class ChooseUser : Window
    {
        bool isDelete;
        public ChooseUser(bool isDelete)
        {
            InitializeComponent();
            this.isDelete = isDelete;
            UserService userService = new UserService();
            DataTable userTable = userService.getUsers();
            foreach (DataRow userRow in userTable.Rows)
            {
                users.Items.Add(userRow[0]);
            }
            users.SelectedIndex = 0;
        }

        private void confirm(object sender, RoutedEventArgs e)
        {
            UserService userService = new UserService();
            string userId = users.SelectedItem.ToString();
            if (isDelete)
            {
                userService.deleteUser((int)Int64.Parse(userId));
            }
            else
            {
                DataTable user = userService.getUserDetails(userId, true);
                UserDetails userDetailsEditWindow = new UserDetails(user, "");
                userDetailsEditWindow.ShowDialog();
            }
            this.Close();
        }

        private void cancel(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
