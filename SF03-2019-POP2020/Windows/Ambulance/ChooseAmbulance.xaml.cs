﻿using SF03_2019_POP2020.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF03_2019_POP2020.Windows.Ambulance
{
    /// <summary>
    /// Interaction logic for ChooseAmbulance.xaml
    /// </summary>
    public partial class ChooseAmbulance : Window
    {
        bool isDelete;
        public ChooseAmbulance(bool isDelete)
        {
            InitializeComponent();
            this.isDelete = isDelete;
            AmbulanceService ambulanceService = new AmbulanceService();
            DataTable ambulanceTable = ambulanceService.getAmbulances();
            foreach (DataRow ambulanceRow in ambulanceTable.Rows)
            {
                ambulances.Items.Add(ambulanceRow[0]);
            }
            ambulances.SelectedIndex = 0;
        }

        private void cancel(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void confirm(object sender, RoutedEventArgs e)
        {
            AmbulanceService ambulanceService = new AmbulanceService();
            string ambulanceId = ambulances.SelectedItem.ToString();
            if (isDelete)
            {
                ambulanceService.deleteAmbulance((int)Int64.Parse(ambulanceId));
            }
            else
            {
                DataTable ambulance = ambulanceService.getAmbulance((int)Int64.Parse(ambulanceId));
                AmbulanceDetails ambulanceDetailsWindow = new AmbulanceDetails(ambulance);
                ambulanceDetailsWindow.ShowDialog();
            }
            this.Close();
        }
    }
}
