﻿using SF03_2019_POP2020.Models;
using SF03_2019_POP2020.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF03_2019_POP2020.Windows.Ambulance
{
    /// <summary>
    /// Interaction logic for AmbulanceDetails.xaml
    /// </summary>
    public partial class AmbulanceDetails : Window
    {
        bool isEdit;
        int ambulanceId;
        public AmbulanceDetails(DataTable ambulance)
        {
            InitializeComponent();
            isEdit = ambulance != null;
            if (isEdit)
            {
                ambulanceId = (int)ambulance.Rows[0]["id"];
            }
            AddressService addressService = new AddressService();
            DataTable addressTable = addressService.getAddresses();
            int userAddress = 0;
            foreach (DataRow addressRow in addressTable.Rows)
            {
                address.Items.Add(addressRow[0]);
            }
            address.SelectedIndex = userAddress;
            name.Text = ambulance != null ? ambulance.Rows[0]["name"].ToString() : "";
        }

        private void cancel(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void save(object sender, RoutedEventArgs e)
        {
            AmbulanceService ambulanceService = new AmbulanceService();
            AmbulanceModel ambulance = new AmbulanceModel();
            ambulance.Name = name.Text;
            ambulance.Address.StreetAndNumber = address.SelectedItem.ToString();
            bool success = false;
            if (ambulanceService.isAmbulanceValid(ambulance))
            {
                if (isEdit)
                {
                    ambulance.Id = ambulanceId;
                    success = ambulanceService.updateAmbulance(ambulance);
                }
                else
                {
                    success = ambulanceService.addAmbulance(ambulance);
                }
            }
            else
            {
                error.Content = "Invalid data!";
            }
            if (success)
            {
                this.Close();
            }
        }
    }
}
