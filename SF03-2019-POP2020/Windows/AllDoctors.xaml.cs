﻿using SF03_2019_POP2020.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF03_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AllDoctors.xaml
    /// </summary>
    public partial class AllDoctors : Window
    {
        ICollectionView view;
        public AllDoctors()
        {
            InitializeComponent();
            UpdateView();

            view.Filter = CustomFilter;

        }
        private bool CustomFilter(object obj)
        {
            User user = obj as User;

            if (user.UserType.Equals(EUserType.DOCTOR) && user.Active)
                if (TxtSearch.Text != "")
                {
                    return user.Name.Contains(TxtSearch.Text);
                }
                else
                    return true;
            return false;
        }
        private void UpdateView()
        {
            //view = CollectionViewSource.GetDefaultView(Util.Instance.Users);
            DGDoctors.ItemsSource = view;
            DGDoctors.IsSynchronizedWithCurrentItem = true;
            DGDoctors.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);

        }

        private void DGDoctors_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("Active") || e.PropertyName.Equals("Error"))
                e.Column.Visibility = Visibility.Collapsed;
        }
        private void MIAddDoctor_Click(object sender, RoutedEventArgs e)
        {
            User newUser = new User();

            AddEditDoctor add = new AddEditDoctor(newUser);

            this.Hide();
            if (!(bool)add.ShowDialog())
            {

            }
            this.Show();
            view.Refresh();
        }
        private void MIEditDoctor_Click(object sender, RoutedEventArgs e)
        {
            User selected = view.CurrentItem as User;
            User odlDoctor = selected.Clone();

            AddEditDoctor add = new AddEditDoctor(selected, EStatus.EDIT);

            this.Hide();
            if (!(bool)add.ShowDialog())
            {
                //int index = Util.Instance.Users.ToList().FindIndex(u => u.Username.Equals(selected.Username));
                //Util.Instance.Users[index] = odlDoctor;
            }
            this.Show();
            view.Refresh();
        }
        private void DeleteDocMI_Click(object sender, RoutedEventArgs e)
        {
            User selected = view.CurrentItem as User;
            //Util.Instance.DeleteUser(selected.Username);

            view.Refresh();
        }

        private void TxtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            view.Refresh();
        }
    }

}
