﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SF03_2019_POP2020.Models;

namespace SF03_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AddEditDoctor.xaml
    /// </summary>
    public partial class AddEditDoctor : Window
    {
        private EStatus selectedStatus;
        private User selectedDoctor;

        public AddEditDoctor(User doctor, EStatus status = EStatus.ADD)
        {
            InitializeComponent();

            this.DataContext = doctor;

            selectedDoctor = doctor;
            selectedStatus = status;

            CmbUserTipe.ItemsSource = Enum.GetValues(typeof(EUserType)).Cast<EUserType>();

            if (status.Equals(EStatus.EDIT) && doctor != null)
            {
                this.Title = "Edit Doctor";
                /*TxtEmail.Text = lekar.Email;
                TxtKorisnickoIme.Text = lekar.KorisnickoIme;
                TxtName.Text = lekar.Ime;
                TxtPrezime.Text = lekar.Prezime;*/
                TxtUsername.IsEnabled = false;
            }
            else
            {
                this.Title = "Add doctor";
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {

            this.DialogResult = false;
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            //if (IsValid())
            //{
            //    if (selectedStatus.Equals(EStatus.ADD))
            //    {
            //        selectedDoctor.Active = true;
            //        Doctor doctor = new Doctor
            //        {
            //            Ambulance = "Ambulance 1",
            //            UserNejm = selectedDoctor
            //        };

            //        Util.Instance.Users.Add(selectedDoctor);
            //        Util.Instance.Doctors.Add(doctor);
            //    }
            //    else
            //    {
            //        //    int editDoctor = Util.Instance.Doctors.ToList().FindIndex(u => u.UserNejm.Username.Equals(TxtUsername.Text));
            //        //    int editUser = Util.Instance.Users.ToList().FindIndex(u => u.Username.Equals(TxtUsername.Text));

            //        //    Util.Instance.Users[editUser] = u;
            //        //    Util.Instance.Doctors[editDoctor] = doctor;
            //        //}

            //        Util.Instance.SaveEnt("users.txt");
            //        Util.Instance.SaveEnt("doctors.txt");

            //        this.DialogResult = true;
            //        this.Close();
            //    }

            //}
        }
        private bool IsValid()
        {
            return !Validation.GetHasError(TxtUsername) && !Validation.GetHasError(TxtEmail)
                && !Validation.GetHasError(TxtName);
        }
    }
}