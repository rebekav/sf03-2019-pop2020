﻿using SF03_2019_POP2020.Models;
using SF03_2019_POP2020.Services;
using SF03_2019_POP2020.Windows.Users;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF03_2019_POP2020.Windows.Patient
{
    /// <summary>
    /// Interaction logic for PatientMain.xaml
    /// </summary>
    public partial class PatientMain : Window
    {
        private string jmbg;
        private DataTable user;

        public PatientMain(string jmbg)
        {
            InitializeComponent();
            this.jmbg = jmbg;
            UserService userService = new UserService();
            this.user = userService.getUserDetails(jmbg, false);
            UserDetails.ItemsSource = user.DefaultView;
        }

        private void edit(object sender, RoutedEventArgs e)
        {
            UserDetails userDetailsEditWindow = new UserDetails(user, EUserType.PATIENT.ToString());
            userDetailsEditWindow.ShowDialog();
            UserService userService = new UserService();
            this.user = userService.getUserDetails(jmbg, false);
            UserDetails.ItemsSource = user.DefaultView;
        }

        private void logOut(object sender, RoutedEventArgs e)
        {
            HomeWindow homeWindow = new HomeWindow();
            this.Close();
            homeWindow.Show();
        }
    }
}
