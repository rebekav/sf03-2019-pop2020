﻿using SF03_2019_POP2020.Models;
using SF03_2019_POP2020.Services;
using SF03_2019_POP2020.Windows.Ambulance;
using SF03_2019_POP2020.Windows.Users;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF03_2019_POP2020.Windows.Admin
{
    public partial class AdminMain : Window
    {
        private string jmbg;
        private DataTable user;

        public AdminMain(string jmbg)
        {
            InitializeComponent();
            this.jmbg = jmbg;
            UserService userService = new UserService();
            this.user = userService.getUserDetails(jmbg, false);
            UserDetails.ItemsSource = user.DefaultView;
        }

        private void edit(object sender, RoutedEventArgs e)
        {
            UserDetails userDetailsEditWindow = new UserDetails(user, EUserType.ADMIN.ToString());
            userDetailsEditWindow.ShowDialog();
            UserService userService = new UserService();
            this.user = userService.getUserDetails(jmbg, false);
            UserDetails.ItemsSource = user.DefaultView;
        }

        private void logOut(object sender, RoutedEventArgs e)
        {
            HomeWindow homeWindow = new HomeWindow();
            this.Close();
            homeWindow.Show();
        }

        private void addUser(object sender, RoutedEventArgs e)
        {
            ChooseUserType chooseUserTypeWindow = new ChooseUserType();
            chooseUserTypeWindow.ShowDialog();
        }

        private void editUser(object sender, RoutedEventArgs e)
        {
            ChooseUser chooseUserWindow = new ChooseUser(false);
            chooseUserWindow.ShowDialog();
        }

        private void deleteUser(object sender, RoutedEventArgs e)
        {
            ChooseUser chooseUserWindow = new ChooseUser(true);
            chooseUserWindow.ShowDialog();
        }

        private void viewUsers(object sender, RoutedEventArgs e)
        {
            AllItems allItemsWindow = new AllItems("USER");
            allItemsWindow.Show();
        }

        private void addAmbulance(object sender, RoutedEventArgs e)
        {
            AmbulanceDetails ambulanceDetailsWindow = new AmbulanceDetails(null);
            ambulanceDetailsWindow.ShowDialog();
        }

        private void editAmbulance(object sender, RoutedEventArgs e)
        {
            ChooseAmbulance chooseAmbulanceWindow = new ChooseAmbulance(false);
            chooseAmbulanceWindow.ShowDialog();
        }

        private void deleteAmbulance(object sender, RoutedEventArgs e)
        {
            ChooseAmbulance chooseAmbulanceWindow = new ChooseAmbulance(true);
            chooseAmbulanceWindow.ShowDialog();
        }

        private void viewAmbulances(object sender, RoutedEventArgs e)
        {
            AllItems allItemsWindow = new AllItems("AMBULANCE");
            allItemsWindow.Show();
        }

        private void addTherapy(object sender, RoutedEventArgs e)
        {

        }

        private void editTherapy(object sender, RoutedEventArgs e)
        {

        }

        private void deleteTherapy(object sender, RoutedEventArgs e)
        {

        }

        private void viewTherapies(object sender, RoutedEventArgs e)
        {

        }

        private void addAppointment(object sender, RoutedEventArgs e)
        {

        }

        private void editAppointment(object sender, RoutedEventArgs e)
        {

        }

        private void deleteAppointment(object sender, RoutedEventArgs e)
        {

        }

        private void viewAppointments(object sender, RoutedEventArgs e)
        {

        }

        private void addAddress(object sender, RoutedEventArgs e)
        {

        }

        private void editAddress(object sender, RoutedEventArgs e)
        {

        }

        private void deleteAddress(object sender, RoutedEventArgs e)
        {

        }

        private void viewAddresses(object sender, RoutedEventArgs e)
        {

        }
    }
}
