﻿using SF03_2019_POP2020.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF03_2019_POP2020.Windows
{
    /// <summary>
    /// Interaction logic for AllItems.xaml
    /// </summary>
    public partial class AllItems : Window
    {
        public AllItems(string type)
        {
            InitializeComponent();
            switch (type)
            {
                case "USER":
                    UserService userService = new UserService();
                    items.ItemsSource = userService.getUsers().DefaultView;
                    break;
                case "AMBULANCE":
                    AmbulanceService ambulanceService = new AmbulanceService();
                    items.ItemsSource = ambulanceService.getAmbulances().DefaultView;
                    break;
            }
        }

        private void back(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
