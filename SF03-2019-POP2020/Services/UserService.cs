﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using SF03_2019_POP2020.Models;
using SF03_2019_POP2020.MyExceptions;



namespace SF03_2019_POP2020.Services
{
    public class UserService : IUserService
    {
        public bool isUserValid(User user)
        {
            long result;
            if (!Int64.TryParse(user.JMBG, out result) || user.JMBG.Length != 13)
            {
                Console.WriteLine("JMBG");
                return false;
            }
            if (!user.Email.Contains("@"))
            {
                Console.WriteLine("email");
                return false;
            }
            if (user.Name == "" || user.Surname == "" || user.Email == "" || user.Password == "")
            {
                Console.WriteLine(user.Name);
                Console.WriteLine(user.Surname);
                Console.WriteLine(user.Email);
                Console.WriteLine(user.Password);
                Console.WriteLine("other");
                return false;
            }
            return true;
        }

        public bool deleteUser(int id)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "UPDATE Users set active = 0 where id = @id";
                command.Parameters.Add(new SqlParameter("id", id));
                return command.ExecuteNonQuery() == 1;
            }
        }

        public DataTable getUserDetails(string field, bool isId)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataTable table = new DataTable("UserDetails");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();
                if (isId)
                {
                    command.CommandText = "SELECT * FROM Users WHERE id = @field and active = 1";
                }
                else
                {
                    command.CommandText = "SELECT * FROM Users WHERE jmbg = @field and active = 1";
                }
                command.Parameters.Add(new SqlParameter("field", field));
                adapter.SelectCommand = command;
                adapter.Fill(table);
                return table;
            }
        }

        public string logIn(string jmbg, string password)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataTable table = new DataTable("UserLogIn");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();
                command.CommandText = "SELECT type FROM Users WHERE jmbg = @jmbg AND password = @password and active = 1";
                command.Parameters.Add(new SqlParameter("jmbg", jmbg));
                command.Parameters.Add(new SqlParameter("password", password));
                string type = (string)command.ExecuteScalar();
                return type;
            }
        }

        public bool registerUser(User user, string type)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT id FROM Address WHERE streetAndNumber = @streetAndNumber and active = 1";
                command.Parameters.Add(new SqlParameter("streetAndNumber", user.Address.StreetAndNumber));
                int addressId = (int)command.ExecuteScalar();

                command = conn.CreateCommand();
                command.CommandText = "INSERT INTO Users (jmbg, name, surname, password, email, address, gender, type, active) " +
                    "VALUES (@jmbg, @name, @surname, @password, @email, @address, @gender, @type, 1)";

                command.Parameters.Add(new SqlParameter("jmbg", Convert.ToInt64(user.JMBG)));
                command.Parameters.Add(new SqlParameter("name", user.Name));
                command.Parameters.Add(new SqlParameter("surname", user.Surname));
                command.Parameters.Add(new SqlParameter("email", user.Email));
                command.Parameters.Add(new SqlParameter("password", user.Password));
                command.Parameters.Add(new SqlParameter("gender", user.Gender.ToString()));
                command.Parameters.Add(new SqlParameter("address", addressId));
                command.Parameters.Add(new SqlParameter("type", type));

                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool updateUser(User user)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = "SELECT id FROM Address WHERE streetAndNumber = @streetAndNumber and active = 1";
                    command.Parameters.Add(new SqlParameter("streetAndNumber", user.Address.StreetAndNumber));
                    int addressId = (int)command.ExecuteScalar();

                    command = conn.CreateCommand();
                    command.CommandText = "UPDATE Users SET name = @name, surname = @surname, email = @email, password = @password, gender = @gender, address = @address WHERE jmbg = @jmbg";

                    command.Parameters.Add(new SqlParameter("jmbg", Convert.ToInt64(user.JMBG)));
                    command.Parameters.Add(new SqlParameter("name", user.Name));
                    command.Parameters.Add(new SqlParameter("surname", user.Surname));
                    command.Parameters.Add(new SqlParameter("email", user.Email));
                    command.Parameters.Add(new SqlParameter("password", user.Password));
                    command.Parameters.Add(new SqlParameter("gender", user.Gender.ToString()));
                    command.Parameters.Add(new SqlParameter("address", addressId));
                    return command.ExecuteNonQuery() == 1;
                }
            }
            catch
            {
                return false;
            }
        }

        public DataTable getUsers()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataTable table = new DataTable();
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();
                command.CommandText = "SELECT * FROM Users WHERE active = 1";
                adapter.SelectCommand = command;
                adapter.Fill(table);
                return table;
            }
        }
    }
}
