﻿using SF03_2019_POP2020.Interfaces;
using SF03_2019_POP2020.Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace SF03_2019_POP2020.Services
{
    public class AppointmentService : IAppointmentService
    {
        public bool addAppointment(Appointment appointment)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM Users WHERE jmbg = @jmbg and active = 1";
                command.Parameters.Add(new SqlParameter("jmbg", appointment.Doctor.JMBG));
                int doctorId = (int)command.ExecuteScalar();
                command = conn.CreateCommand();
                command.CommandText = "INSERT INTO Appointment (doctorId, date, status, patientId, active) values (@doctorId, @date, @status, @patientId, 1)";
                command.Parameters.Add(new SqlParameter("doctorId", doctorId));
                command.Parameters.Add(new SqlParameter("date", appointment.Date));
                command.Parameters.Add(new SqlParameter("status", appointment.Patient == null ? EStatusApp.FREE : EStatusApp.SCHEDULED));
                command.Parameters.Add(new SqlParameter("patientId", appointment.Patient?.Id));
                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool deleteAppointment(int id)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "UPDATE Appointment SET active = 0 WHERE id = @id";
                command.Parameters.Add(new SqlParameter("id", id));
                return command.ExecuteNonQuery() == 1;
            }
        }

        public DataTable getAppointments()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataTable table = new DataTable("Appointment");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();
                command.CommandText = "SELECT * FROM Appointment WHERE active = 1";
                adapter.SelectCommand = command;
                adapter.Fill(table);
                return table;
            }
        }

        public bool updateAppointment(Appointment appointment)
        {
            throw new NotImplementedException();
        }
    }
}
