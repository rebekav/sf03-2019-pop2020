﻿using SF03_2019_POP2020.Interfaces;
using SF03_2019_POP2020.Models;
using System.Data;
using System.Data.SqlClient;

namespace SF03_2019_POP2020.Services
{
    public class AmbulanceService : IAmbulanceService
    {
        public bool addAmbulance(AmbulanceModel ambulance)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT id FROM Address WHERE streetAndNumber = @streetAndNumber and active = 1";
                command.Parameters.Add(new SqlParameter("streetAndNumber", ambulance.Address.StreetAndNumber));
                int addressId = (int)command.ExecuteScalar();
                command = conn.CreateCommand();
                command.CommandText = "INSERT INTO Ambulance (name, address, active) VALUES (@name, @address, 1)";
                command.Parameters.Add(new SqlParameter("name", ambulance.Name));
                command.Parameters.Add(new SqlParameter("address", addressId));
                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool deleteAmbulance(int id)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "UPDATE Ambulance set active = 0 where id = @id";
                command.Parameters.Add(new SqlParameter("id", id));
                return command.ExecuteNonQuery() == 1;
            }
        }

        public DataTable getAmbulance(int id)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataTable table = new DataTable("Ambulance");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();
                command.CommandText = "SELECT * FROM Ambulance WHERE id = @id and active = 1";
                command.Parameters.Add(new SqlParameter("id", id));
                adapter.SelectCommand = command;
                adapter.Fill(table);
                return table;
            }
        }

        public DataTable getAmbulances()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataTable table = new DataTable("Ambulance");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();
                command.CommandText = "SELECT * FROM Ambulance WHERE active = 1";
                adapter.SelectCommand = command;
                adapter.Fill(table);
                return table;
            }
        }

        public bool isAmbulanceValid(AmbulanceModel ambulance)
        {
            if (ambulance.Name == "")
            {
                return false;
            }
            return true;
        }

        public bool updateAmbulance(AmbulanceModel ambulance)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = "SELECT id FROM Address WHERE streetAndNumber = @streetAndNumber and active = 1";
                    command.Parameters.Add(new SqlParameter("streetAndNumber", ambulance.Address.StreetAndNumber));
                    int addressId = (int)command.ExecuteScalar();

                    command.CommandText = "UPDATE Ambulance SET name = @name, address = @address WHERE id = @id";
                    command.Parameters.Add(new SqlParameter("name", ambulance.Name));
                    System.Console.WriteLine(ambulance.Name);
                    command.Parameters.Add(new SqlParameter("address", addressId));
                    System.Console.WriteLine(addressId);
                    command.Parameters.Add(new SqlParameter("id", ambulance.Id));
                    System.Console.WriteLine(ambulance.Id);
                    System.Console.WriteLine("rows: " + command.ExecuteNonQuery());
                    return command.ExecuteNonQuery() == 1;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
