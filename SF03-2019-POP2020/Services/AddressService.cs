﻿using SF03_2019_POP2020.Models;
using System.Data;
using System.Data.SqlClient;

namespace SF03_2019_POP2020.Services
{
    public class AddressService : IAddressService
    {
        public bool addAddress(Address address)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "INSERT INTO Address (streetAndNumber, city, country, active) " +
                    "VALUES (@street, @number, @city, @country, 1)";
                command.Parameters.Add(new SqlParameter("street", address.StreetAndNumber));
                command.Parameters.Add(new SqlParameter("city", address.City));
                command.Parameters.Add(new SqlParameter("country", address.Country));
                command.Parameters.Add(new SqlParameter("id", address.Id));
                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool deleteAddress(int id)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "UPDATE Address set active = 0 WHERE id = @id";
                command.Parameters.Add(new SqlParameter("id", id));
                return command.ExecuteNonQuery() == 1;
            }
        }

        public DataTable getAddresses()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataTable table = new DataTable("Address");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();
                command.CommandText = "SELECT streetAndNumber FROM Address WHERE active = 1";
                adapter.SelectCommand = command;
                adapter.Fill(table);
                return table;
            }
        }

        public bool updateAddress(Address address)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
                {
                    conn.Open();
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = "UPDATE Address " +
                        "SET streetAndNumber = @streetAndNumber, city = @city, country = @country WHERE id = @id";
                    command.Parameters.Add(new SqlParameter("street", address.StreetAndNumber));
                    command.Parameters.Add(new SqlParameter("city", address.City));
                    command.Parameters.Add(new SqlParameter("country", address.Country));
                    command.Parameters.Add(new SqlParameter("id", address.Id));
                    return command.ExecuteNonQuery() == 1;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
