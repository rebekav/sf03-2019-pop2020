﻿using SF03_2019_POP2020.Interfaces;
using SF03_2019_POP2020.Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace SF03_2019_POP2020.Services
{
    public class TherapyService : ITherapyService
    {
        public bool addTherapy(Therapy therapy)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "SELECT * FROM Users WHERE jmbg = @jmbg and active = 1";
                command.Parameters.Add(new SqlParameter("jmbg", therapy.Doctor.JMBG));
                int doctorId = (int)command.ExecuteScalar();
                command = conn.CreateCommand();
                command.CommandText = "INSERT INTO Therapy (description, doctorId, active) VALUES (@description, @doctorId, 1)";
                command.Parameters.Add(new SqlParameter("description", therapy.Description));
                command.Parameters.Add(new SqlParameter("idLekara", doctorId));
                return command.ExecuteNonQuery() == 1;
            }
        }

        public bool deleteTherapy(int id)
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                SqlCommand command = conn.CreateCommand();
                command.CommandText = "UPDATE Therapy SET active = 0 WHERE id = @id";
                command.Parameters.Add(new SqlParameter("id", id));
                return command.ExecuteNonQuery() == 1;
            }
        }

        public DataTable getTherapies()
        {
            using (SqlConnection conn = new SqlConnection(Data.ConnectionString))
            {
                conn.Open();
                DataTable table = new DataTable("Therapy");
                SqlCommand command = conn.CreateCommand();
                SqlDataAdapter adapter = new SqlDataAdapter();
                command.CommandText = "SELECT * FROM Therapy WHERE active = 1";
                adapter.SelectCommand = command;
                adapter.Fill(table);
                return table;
            }
        }

        public bool updateTherapy(Therapy therapy)
        {
            throw new NotImplementedException();
        }
    }
}
